#/usr/bin/python
"""
Transform a string to a look-alike utf-8.

> https://github.com/woodgern/confusables
> https://stackoverflow.com/questions/46871728/similar-looking-utf8-characters-for-ascii
"""
import argparse
import random
from confusables import confusable_characters


def gen_confuse(input):
    accumulator = []
    for letter in input:
        confs = confusable_characters(letter)
        accumulator.append(random.choices(confs).pop())

    return ''.join(accumulator)


def main(input, iteration):
    for _ in range(0, iteration):
        print(gen_confuse(input))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-input", "--input", help="input to be transformed")
    parser.add_argument("-n", "--iteration", type=int, default=1, nargs='?', help="how many times to do the transform")
    args = parser.parse_args()

    main(input=args.input, iteration=args.iteration)
